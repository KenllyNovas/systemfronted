import React, {useEffect, useState} from 'react';
import './App.css';
import axios from 'axios';
import {makeStyles} from '@material-ui/core/styles';
import {Table, TableContainer, TableHead, TableCell, TableBody, TableRow, Modal, Button, TextField} from '@material-ui/core';
import {Edit, Delete} from '@material-ui/icons';

const baseUrl='http://localhost:3001/users/'

const useStyles = makeStyles((theme) => ({
  modal: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  iconos:{
    cursor: 'pointer'
  }, 
  inputMaterial:{
    width: '100%'
  }
}));

const  App=()=> {
const styles= useStyles();
  const [data, setData]=useState([]);
  const [modalInsertar, setModalInsertar]=useState(false);
  const [modalEditar, setModalEditar]=useState(false);
  const [modalEliminar, setModalEliminar]=useState(false);

  const [userSeleccionada, setuserSeleccionada]=useState({
    nombre: '',
    correo:'',
    edad: '',
    password: '',
    rol: '',
  })

  const handleChange=e=>{
    const {name, value}=e.target;
    setuserSeleccionada(prevState=>({
      ...prevState,
      [name]: value
    }))
    console.log(userSeleccionada);
  }

  const peticionGet=async()=>{
    await axios.get(baseUrl)
    .then(response=>{
      setData(response.data);
    })
  }

  const peticionPost=async()=>{
    await axios.post(baseUrl, userSeleccionada)
    .then(response=>{
      setData(data.concat(response.data))
      abrirCerrarModalInsertar()
    })
  }

  const peticionPut=async()=>{
    await axios.put(baseUrl+userSeleccionada.id, userSeleccionada)
    .then(response=>{
      var dataNueva=data;
      dataNueva.map(user=>{
        if(userSeleccionada.id===user.id){
          user.nombre=userSeleccionada.nombre;
          user.lanzamiento=userSeleccionada.lanzamiento;
          user.empresa=userSeleccionada.empresa;
          user.unidades_vendidas=userSeleccionada.unidades_vendidas;
        }
      })
      setData(dataNueva);
      abrirCerrarModalEditar();
    })
  }

  const peticionDelete=async()=>{
    await axios.delete(baseUrl+userSeleccionada.id)
    .then(response=>{
      setData(data.filter(user=>user.id!==userSeleccionada.id));
      abrirCerrarModalEliminar();
    })
  }

  const abrirCerrarModalInsertar=()=>{
    setModalInsertar(!modalInsertar);
  }

  const abrirCerrarModalEditar=()=>{
    setModalEditar(!modalEditar);
  }

  const abrirCerrarModalEliminar=()=>{
    setModalEliminar(!modalEliminar);
  }

  const seleccionaruser=(user, caso)=>{
    setuserSeleccionada(user);
    (caso==='Editar')?abrirCerrarModalEditar():abrirCerrarModalEliminar()
  }

  useEffect(async()=>{
    await peticionGet();
  },[])

  const bodyInsertar=(
    <div className={styles.modal}>
      <h3>Agregar Nueva user</h3>
      <TextField name="nombre" className={styles.inputMaterial} label="Nombre" onChange={handleChange}/>
      <br />
      <TextField name="correo" className={styles.inputMaterial} label="Correo" onChange={handleChange}/>
      <br />
      <TextField name="edad" className={styles.inputMaterial} label="Edad" onChange={handleChange}/>
      <br />
      <TextField name="password" className={styles.inputMaterial} label="Contreseña" onChange={handleChange}/>
      <br />
      <TextField name="rol" className={styles.inputMaterial} label="Rol" onChange={handleChange}/>
      <br /><br />
      <div align="right">
        <Button color="primary" onClick={()=>peticionPost()}>Insertar</Button>
        <Button onClick={()=>abrirCerrarModalInsertar()}>Cancelar</Button>
      </div>
    </div>
  )

  const bodyEditar=(
    <div className={styles.modal}>
      <h3>Editar user</h3>
      <TextField name="nombre" className={styles.inputMaterial} label="Nombre" onChange={handleChange} value={userSeleccionada && userSeleccionada.nombre}/>
      <br />
      <TextField name="correo" className={styles.inputMaterial} label="Correo" onChange={handleChange} value={userSeleccionada && userSeleccionada.correo}/>
      <br />
      <TextField name="edad" className={styles.inputMaterial} label="Edad" onChange={handleChange} value={userSeleccionada && userSeleccionada.edad}/>
      <br />
      <TextField name="password" className={styles.inputMaterial} label="Contreseña" onChange={handleChange}/>
      <br />
      <TextField name="rol" className={styles.inputMaterial} label="Rol" onChange={handleChange}/>
      <br /><br />
      <div align="right">
        <Button color="primary" onClick={()=>peticionPut()}>Editar</Button>
        <Button onClick={()=>abrirCerrarModalEditar()}>Cancelar</Button>
      </div>
    </div>
  )

  const bodyEliminar=(
    <div className={styles.modal}>
      <p>Estás seguro que deseas eliminar el Usuario<b>{userSeleccionada && userSeleccionada.nombre}</b> ? </p>
      <div align="right">
        <Button color="secondary" onClick={()=>peticionDelete()} >Sí</Button>
        <Button onClick={()=>abrirCerrarModalEliminar()}>No</Button>

      </div>

    </div>
  )


  return (
    <div className="App">
      <br />
    <Button onClick={()=>abrirCerrarModalInsertar()}>Insertar</Button>
      <br /><br />
     <TableContainer>
       <Table>
         <TableHead>
           <TableRow>
             <TableCell>Nombre</TableCell>
             <TableCell>Correo</TableCell>
             <TableCell>Edad</TableCell>
             <TableCell>Acciones</TableCell>
           </TableRow>
         </TableHead>

         <TableBody>
           {data.map(user=>(
             <TableRow key={user.id}>
               <TableCell>{user.nombre}</TableCell>
               <TableCell>{user.correo}</TableCell>
               <TableCell>{user.edad}</TableCell>
               <TableCell>
                 <Edit className={styles.iconos} onClick={()=>seleccionaruser(user, 'Editar')}/>
                 &nbsp;&nbsp;&nbsp;
                 <Delete  className={styles.iconos} onClick={()=>seleccionaruser(user, 'Eliminar')}/>
                 </TableCell>
             </TableRow>
           ))}
         </TableBody>
       </Table>
     </TableContainer>
     
     <Modal
     open={modalInsertar}
     onClose={abrirCerrarModalInsertar}>
        {bodyInsertar}
     </Modal>

     <Modal
     open={modalEditar}
     onClose={abrirCerrarModalEditar}>
        {bodyEditar}
     </Modal>

     <Modal
     open={modalEliminar}
     onClose={abrirCerrarModalEliminar}>
        {bodyEliminar}
     </Modal>
    </div>
  );
}

export default App;
